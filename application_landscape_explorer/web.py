from pathlib import Path

import aiohttp_jinja2
import jinja2
from aiohttp import web
from aiohttp_remotes import XForwardedRelaxed

from application_landscape_explorer import __version__

CONFIG = "config"

routes = web.RouteTableDef()


@routes.get("/")
@aiohttp_jinja2.template("index.html")
async def get_index(request):
    if request.app[CONFIG].applications_file or request.app[CONFIG].dependencies_file:
        return {
            "applications_path": "data/applications.tsv",
            "dependencies_path": "data/dependencies.tsv",
        }
    else:
        # demo with example data
        return {}


@routes.get("/data/{path}")
async def get_data(request):
    path = request.match_info["path"]
    if "applications" in path:
        file_path = request.app[CONFIG].applications_file
    else:
        file_path = request.app[CONFIG].dependencies_file
    if file_path:
        text = file_path.read_text()
        if "dependencies" in path and not text.startswith("source"):
            # add TSV header if missing
            text = "source\ttarget\n" + text
    else:
        text = ""
    return web.Response(text=text)


def get_app(config):
    templates_paths = [str(Path(__file__).parent / "templates")]
    if config.templates_path:
        # prepend the custom template path so custom templates will overwrite any default ones
        templates_paths.insert(0, config.templates_path)

    static_assets_path = Path(__file__).parent / "templates" / "assets"
    if config.static_assets_path:
        # overwrite assets path
        static_assets_path = Path(config.static_assets_path)

    app = web.Application()
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(templates_paths),
        trim_blocks=True,
        lstrip_blocks=True,
    )
    env = aiohttp_jinja2.get_env(app)
    env.globals["version"] = __version__

    app.add_routes(routes)
    app.router.add_static("/assets", static_assets_path)

    # behind proxy
    app.middlewares.append(XForwardedRelaxed().middleware)

    app[CONFIG] = config

    return app
