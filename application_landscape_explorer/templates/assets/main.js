const width = 500
const height = 500

const NODE_RADIUS = 5
const NODE_RADIUS_HOVER = 8
const DEPENDENCY_STROKE_WIDTH = 1

// available URL parameters (#param1=value1;p2=v2)
const PARAM_Q = "q"
const PARAM_SCALE = "scale"
const PARAM_PRUNE = "prune"
const PARAM_PARTITIONX = "partitionx"
const PARAM_PARTITIONY = "partitiony"
const PARAM_NODECOLOR = "nodecolor"
const PARAM_GROUPBY = "groupby"

const nodesById = new Map();
const nodesByTeam = new Map();
const nodesByTag = new Map()

const selectedNodes = new Set();

/**
 * Calculate a 32 bit FNV-1a hash
 * Found here: https://gist.github.com/vaiorabbit/5657561
 * Ref.: http://isthe.com/chongo/tech/comp/fnv/
 *
 * @param {string} str the input value
 * @param {boolean} [asString=false] set to true to return the hash value as
 *     8-digit hex string instead of an integer
 * @param {integer} [seed] optionally pass the hash of the previous chunk
 * @returns {integer | string}
 */
function hashFnv32a (str, asString, seed) {
    /*jshint bitwise:false */
    var i, l,
        hval = (seed === undefined) ? 0x811c9dc5 : seed;

    for (i = 0, l = str.length; i < l; i++) {
        hval ^= str.charCodeAt(i);
        hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
    }
    if (asString) {
        // Convert to 8 digit hex string
        return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
    }
    return hval >>> 0;
}

const colors = new Map()

function hashStringToColor (str) {
    if (colors.has(str)) {
        return colors.get(str)
    } else {
        const hash = hashFnv32a(str);
        const color = "hsl(" + (hash % 360) + ",67%,61%)"
        colors.set(str, color)
        return color
    }
}


function parseLocationHash () {
    // hash startswith #
    const hash = document.location.hash.substring(1)
    const params = new Map()
    for (const pair of hash.split(';')) {
        const keyValue = pair.split('=', 2)
        if (keyValue.length == 2) {
            params.set(keyValue[0], decodeURIComponent(keyValue[1]))
        }
    }
    return params
}

function changeLocationHash (key, value) {
    const params = parseLocationHash()
    params.set(key, value)
    const pairs = []
    for (const [key, value] of params) {
        if (value) {
            pairs.push(key + '=' + encodeURIComponent(value))
        }
    }

    document.location.hash = '#' + pairs.sort().join(';')
}


drag = simulation => {

    function dragstarted (d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged (d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended (d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

    return d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
}

chart = function(data) {

    data.nodes.forEach(node => {
        nodesById.set(node.id, node)
        if (!nodesByTeam.has(node.team)) {
            nodesByTeam.set(node.team, new Set());
        }
        nodesByTeam.get(node.team).add(node.id);
        node.tags.forEach(tag => {
            if (!nodesByTag.has(tag)) {
                nodesByTag.set(tag, new Set())
            }
            nodesByTag.get(tag).add(node.id)
        })

    })
    const links = data.links.map(d => Object.create(d));
    const originalLinks = Array.from(links)
    const nodes = data.nodes.map(d => Object.create(d));
    const originalNodes = Array.from(nodes)

    const simulation = d3.forceSimulation(nodes)
        .force("link", d3.forceLink(links).id(d => d.id))
        .force("collision", d3.forceCollide(NODE_RADIUS * 1.2))
        .force("charge", d3.forceManyBody().distanceMax(800))
        .force("center", d3.forceCenter(width / 2, height / 2))

    const params = parseLocationHash()
    if (params.has(PARAM_PARTITIONX)) {
        const partitionx = params.get(PARAM_PARTITIONX).split(",")
        const tagX = new Map()
        partitionx.forEach((t, i) => {
            tagX.set(t, i * (width / Math.max(1, partitionx.length - 1)))
        })
        simulation.force("partitionx", d3.forceX().x(d => {
            var x = width / 2
            d.tags.forEach(t => {
                if (tagX.has(t)) {
                    x = tagX.get(t)
                }
            })
            return x
        }).strength(1))
    }
    if (params.has(PARAM_PARTITIONY)) {
        const partitiony = params.get(PARAM_PARTITIONY).split(",")
        const tagY = new Map()
        partitiony.forEach((t, i) => {
            tagY.set(t, i * (height / Math.max(1, partitiony.length - 1)))
        })
        simulation.force("partitiony", d3.forceY().y(d => {
            var y = height / 2
            d.tags.forEach(t => {
                if (tagY.has(t)) {
                    y = tagY.get(t)
                }
            })
            return y
        }).strength(1))
    }

    const svg = d3.select("svg")
        .attr("viewBox", [0, 0, width, height]);

    var link = svg.append("g")
        .attr("stroke", "#999")
        .attr("stroke-opacity", 0.6)
        .selectAll(".link")

    var node = svg.append("g")
        .attr("stroke", "#fff")
        .attr("stroke-width", 0.5)
        .selectAll(".node")

    restart()

    const zoom = d3.zoom()
        .extent([
            [0, 0],
            [width, height]
        ])
        .scaleExtent([0.1, 20])
        .on("zoom", zoomed)
        .on("end", zoomEnd)
    svg.call(zoom)

    function zoomed () {
        node.attr("transform", d3.event.transform);
        link.attr("transform", d3.event.transform);
    }

    function zoomEnd () {
        changeLocationHash(PARAM_SCALE, Math.round(d3.event.transform.k * 10000) / 10000)
    }

    node.append("title")
        .text(d => `${d.id} (team: ${d.team})`);

    simulation.on("tick", () => {
        link
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);

        node
            .attr("cx", d => d.x)
            .attr("cy", d => d.y);

    });

    function restart () {

        const nodecolor = params.get(PARAM_NODECOLOR) || "team"

        var fillFunction

        const unknownColor = "hsl(0,0%,61%)"
        if (nodecolor.startsWith("tag:")) {
            const tagPrefix = nodecolor.split(":")[1] + ":"
            fillFunction = d => {
                var color = unknownColor
                d.tags.forEach(t => {
                    if (t.startsWith(tagPrefix)) {
                        color = hashStringToColor(t.substring(tagPrefix.length))
                    }
                })
                return color
            }
        } else {
            fillFunction = d => {
                if (typeof d.team !== "undefined") {
                    return hashStringToColor(d.team)
                } else {
                    return unknownColor
                }
            }
        }

        // Apply the general update pattern to the nodes.
        node = node
            .data(nodes, d => d.id)
        node.exit().remove()
        node = node.enter().append("circle")
            .attr("r", NODE_RADIUS)
            .attr("nid", d => d.id)
            .attr("fill", fillFunction)
            .on("click", handleClick)
            .on("mouseover", handleMouseOver)
            .on("mouseout", handleMouseOut)
            .call(drag(simulation)).merge(node);

        // Apply the general update pattern to the links.
        link = link
            .data(links, d => `${d.source.id}->${d.type}->${d.target.id}`)
        link.exit().remove()
        link = link.enter().append("line")
            .attr("from-nid", d => d.source.id)
            .attr("to-nid", d => d.target.id)
            .attr("stroke-width", DEPENDENCY_STROKE_WIDTH)
            .attr("marker-end", "url(#arrow-grey)")
            .on("mouseover", handleMouseOverDependency)
            .on("mouseout", handleMouseOutDependency)
            .merge(link)


        // Update and restart the simulation.
        simulation.nodes(nodes);
        simulation.force("link").links(links);
        simulation.alpha(1).restart();
    }


    function centerOnSelection () {
        var left = width * 10
        var right = 0
        var top = height * 10
        var bottom = 0
        selectedNodes.forEach(nodeId => {
            d3.selectAll("circle[nid='" + nodeId + "']").each(d => {
                left = Math.min(left, d.x)
                right = Math.max(right, d.x)
                top = Math.min(top, d.y)
                bottom = Math.max(bottom, d.y)
            })
        })
        svg.transition().call(zoom.translateTo, (right + left) / 2, (bottom + top) / 2)
    }


    d3.select("body").on("keydown", function() {
        if (d3.event.target.localName == "input") {
            // unfocus on enter or escape
            if (d3.event.keyCode == 13 || d3.event.keyCode == 27) {
                search(d3.event.target)
                d3.event.target.blur()
            }
            return
        }
        switch (d3.event.keyCode) {
            case 83: // s
                document.getElementById("search-input").focus()
                d3.event.preventDefault()
                break;
            case 67: // c
                // center on selection
                if (selectedNodes.size == 0) {
                    svg.transition().call(zoom.transform, d3.zoomIdentity)
                } else {
                    centerOnSelection()
                }
                break;
            case 37: // left
                svg.transition().call(zoom.translateBy, 50, 0)
                break
            case 38: // top
                svg.transition().call(zoom.translateBy, 0, 50)
                break
            case 39: // right
                svg.transition().call(zoom.translateBy, -50, 0)
                break
            case 40: // bottom
                svg.transition().call(zoom.translateBy, 0, -50)
                break
            case 171:
            case 187: // Chrome
                // +
                svg.transition().call(zoom.scaleBy, 2)
                break;
            case 173:
            case 189: // Chrome
                // -
                svg.transition().call(zoom.scaleBy, 0.5)
                break;
            case 49:
                // 1
                prune(1)
                break;
            case 50:
                // 2
                prune(2)
                break;
            case 51:
                // 3
                prune(3)
                break;
            case 52:
                // 4
                prune(4)
                break;
            case 53:
                // 5
                prune(5)
                break;
            case 54:
                // 6
                prune(6)
                break;
            case 55:
                prune(7)
                break;
            case 56:
                prune(8)
                break;
            case 57:
                prune(9)
                break;
            case 48:
                prune(0)
                break;
        }
    })

    function prune (level) {
        if (level != 0 && selectedNodes.size < 1) {
            // do nothing if nothing is selected
            return;
        }

        changeLocationHash(PARAM_PRUNE, level)

        var index
        const linkedNodes = new Set()

        links.splice(0, links.length)

        const addedLinks = new Set()

        originalLinks.forEach((link, idx) => {
            if (level == 0 || selectedNodes.has(link.source.id) || selectedNodes.has(link.target.id)) {
                links.push(link)
                addedLinks.add(idx)
                linkedNodes.add(link.source.id)
                linkedNodes.add(link.target.id)
            }
        })

        while (level > 1) {
            // copy
            const currentLinkedNodes = new Set()
            linkedNodes.forEach(a => {
                currentLinkedNodes.add(a)
            })

            originalLinks.forEach((link, idx) => {
                if (!addedLinks.has(idx) && (currentLinkedNodes.has(link.source.id) || currentLinkedNodes.has(link.target.id))) {
                    links.push(link)
                    addedLinks.add(idx)
                    linkedNodes.add(link.source.id)
                    linkedNodes.add(link.target.id)
                }
            })
            level--;
        }


        nodes.splice(0, nodes.length)

        originalNodes.forEach(node => {
            if (level == 0 || selectedNodes.has(node.id) || linkedNodes.has(node.id)) {
                nodes.push(node)
            }
        })
        // redraw
        svg.transition().call(zoom.scaleBy, 1.01)

        restart()
    }



    function handleClick (d) {
        selectNodes([d.id]);
    }

    function handleMouseOver (d, i) {
        d3.select(this).classed("hover", true).transition().attr("r", NODE_RADIUS_HOVER)
        for (const k in d) {
            d3.select(`#application-info *[data-key=${k}]`).text(d[k])
        }
        d3.select("#application-info .tags").selectAll("span").data(Array.from(d.tags)).join("span").attr("class", "tag").text(d => d)
        d3.select("#application-info").style("display", "block").transition().style("opacity", 1)
    }

    function handleMouseOut (d, i) {
        d3.select(this).classed("hover", false).transition().attr("r", NODE_RADIUS)
        d3.select("#application-info").style("opacity", 0).style("display", "none")
    }

    function handleMouseOverDependency (d, i) {
        d3.select(this).attr("stroke-width", 2)
        d3.select("#dependency-info .panel-heading").text(`${d.source.id} -> ${d.target.id}`)
        for (const k in d) {
            d3.select(`#dependency-info *[data-key=${k}]`).text(d[k])
        }
        d3.select("#dependency-info .tags").selectAll("span").data(Array.from(d.tags)).join("span").attr("class", "tag").text(d => d)
        d3.select("#dependency-info").transition().style("opacity", 1)
    }

    function handleMouseOutDependency (d, i) {
        d3.select(this).attr("stroke-width", DEPENDENCY_STROKE_WIDTH)
        d3.select("#dependency-info").transition().style("opacity", 0)
    }

    if (params.has(PARAM_Q)) {
        selectNodesByQuery(params.get(PARAM_Q))
    }
    if (params.has(PARAM_SCALE)) {
        svg.call(zoom.scaleTo, parseFloat(params.get(PARAM_SCALE)))
    }
    if (params.has(PARAM_PRUNE)) {
        prune(parseInt(params.get(PARAM_PRUNE)))
    }
}

function search (obj) {
    const q = obj.value;
    selectNodesByQuery(q);
}

function selectNodes (nodeIds) {
    selectedNodes.clear()
    d3.selectAll("circle").classed("active", false).classed("inactive", nodeIds.length > 0)
    d3.selectAll("line").classed("active", false).classed("inactive", nodeIds.length > 0)
        .attr("marker-end", "url(#arrow-grey)")

    const appProperties = new Map()
    const tags = new Set()
    for (const nodeId of nodeIds) {
        selectedNodes.add(nodeId)
        d3.selectAll("circle[nid='" + nodeId + "']").classed("active", true).classed("inactive", false)
        d3.selectAll("line[from-nid='" + nodeId + "']").classed("active", true).classed("inactive", false)
            .attr("marker-end", "url(#arrow-red)")
        d3.selectAll("line[to-nid='" + nodeId + "']").classed("active", true).classed("inactive", false)
            .attr("marker-end", "url(#arrow-red)")

        const d = nodesById.get(nodeId)
        if (typeof d !== "undefined") {
            for (const k in d) {
                if (!appProperties.has(k)) {
                    appProperties.set(k, new Set())
                }
                appProperties.get(k).add(d[k])
                d3.select(`#selection-info *[data-key=${k}]`).text(d[k])
            }
            d.tags.forEach(t => tags.add(t))
        }
    }

    if (nodeIds.length > 1) {
        appProperties.forEach((v, k) => {
            const arr = Array.from(v)
            const shortened = arr.slice(0, 3)
            var value = shortened.join(", ") + (shortened.length < arr.length ? ` ... (${arr.length})` : "")
            d3.select(`#selection-info *[data-key=${k}]`).text(value)
        })
    }

    if (nodeIds.length > 0) {
        d3.select("#selection-info .tags").selectAll("a").data(Array.from(tags)).join("a").attr("class", "tag").text(d => d).on("click", d => selectNodesByQuery(d))
        d3.select("#selection-info").transition().style("opacity", 1)
    } else {
        d3.select("#selection-info").transition().style("opacity", 0)
    }
}

function selectNodesByQuery (q) {
    d3.select("#search-input").node().value = q
    changeLocationHash(PARAM_Q, q)
    var nodeIds = [];
    if (nodesByTeam.has(q)) {
        nodeIds = Array.from(nodesByTeam.get(q))
    } else if (nodesByTag.has(q)) {
        nodeIds = Array.from(nodesByTag.get(q))
    } else if (q.length > 0) {
        nodeIds = [q]
    }
    selectNodes(nodeIds)
}

function main (config) {

    const data = {
        "nodes": [],
        "links": []
    }

    const params = parseLocationHash()

    const groupby = params.get(PARAM_GROUPBY)

    d3.tsv(config.applications_path).then(function(rows) {
        const appsById = new Map()
        const appsWithLink = new Set()
        const groups = new Map()
        var group
        rows.forEach(row => {
            const tags = new Set((row.tags || "").split(",").filter(t => t.length > 0))
            row.tags = tags
            appsById.set(row.id, row)
            if (groupby == "team") {
                if (!groups.has(row.team)) {
                    groups.set(row.team, new Set())
                }
                group = groups.get(row.team)
                group.add(row)
            } else {
                data.nodes.push(row)
            }
        })
        groups.forEach((g, k) => {
            const tags = new Set()
            var team = ""
            g.forEach(a => {
                // only one value
                team = a.team
                a.tags.forEach(t => {
                    tags.add(t)
                })
            })
            data.nodes.push({
                id: k,
                team: team,
                tags: tags
            })
        })
        const nodeIds = new Set()
        data.nodes.forEach(n => {
            nodeIds.add(n.id)
        })
        d3.tsv(config.dependencies_path).then(function(rows) {
            const groupLinks = new Map()
            rows.forEach(row => {
                const tags = new Set((row.tags || "").split(",").filter(t => t.length > 0))
                row.tags = tags
                if (groupby == "team") {
                    const sourceApp = appsById.get(row.source)
                    const targetApp = appsById.get(row.target)
                    if (typeof sourceApp !== "undefined" && typeof targetApp !== "undefined") {
                        const k = `${sourceApp.team}->${targetApp.team}`
                        if (!groupLinks.has(k)) {
                            groupLinks.set(k, new Set())
                        }
                        groupLinks.get(k).add(row)
                    }
                } else {
                    data.links.push(row)
                    appsWithLink.add(row.source)
                    appsWithLink.add(row.target)
                    if (!nodeIds.has(row.source)) {
                        nodeIds.add(row.source)
                        data.nodes.push({
                            "id": row.source,
                            "tags": new Set()
                        })
                    }
                    if (!nodeIds.has(row.target)) {
                        nodeIds.add(row.target)
                        data.nodes.push({
                            "id": row.target,
                            "tags": new Set()
                        })
                    }
                }
            })
            groupLinks.forEach((g, k) => {
                const tags = new Set()
                const parts = k.split("->")
                const source = parts[0]
                const target = parts[1]
                if (source != target) {
                    data.links.push({
                        source: source,
                        target: target,
                        tags: tags,
                        weight: g.size
                    })
                }
            })
            if (!config.include_nodes_without_links) {
                // filter out all nodes which do not have any dependency
                var index = data.nodes.length - 1
                while (index >= 0) {
                    if (!appsWithLink.has(data.nodes[index].id)) {
                        data.nodes.splice(index, 1);
                    }

                    index -= 1;
                }
            }
            chart(data)
        });
    });
}