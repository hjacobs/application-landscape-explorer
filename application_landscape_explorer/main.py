import argparse
from pathlib import Path

import aiohttp.web

from .web import get_app
from application_landscape_explorer import __version__


def parse_args(argv=None):

    parser = argparse.ArgumentParser(
        description=f"Application Landscape Explorer v{__version__}"
    )
    parser.add_argument(
        "--port",
        type=int,
        default=8080,
        help="TCP port to start webserver on (default: 8080)",
    )
    parser.add_argument(
        "--version",
        action="version",
        version=f"application-landscape-explorer {__version__}",
    )
    # customization options
    parser.add_argument(
        "--templates-path", help="Path to directory with custom HTML/Jinja2 templates"
    )
    parser.add_argument(
        "--static-assets-path",
        help="Path to custom JS/CSS assets (will be mounted as /assets HTTP path)",
    )
    parser.add_argument(
        "--applications-file",
        help="Path to TSV file with application information",
        type=Path,
    )
    parser.add_argument(
        "--dependencies-file",
        help="Path to TSV file with dependencies information",
        type=Path,
    )
    return parser.parse_args()


def main():
    args = parse_args()

    app = get_app(args)
    aiohttp.web.run_app(app, port=args.port, handle_signals=False)
