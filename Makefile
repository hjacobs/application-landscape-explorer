IMAGE            ?= hjacobs/application-landscape-explorer
VERSION          ?= $(shell git describe --tags --always --dirty)
TAG              ?= $(VERSION)


.PHONY: install
install:
	poetry install

.PHONY: lint
lint:
	poetry run pre-commit run --all-files

.PHONY: build
build:
	docker build --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" .
	@echo 'Docker image $(IMAGE):$(TAG) can now be used.'

.PHONY: push
push: build
	docker push "$(IMAGE):$(TAG)"
	docker tag "$(IMAGE):$(TAG)" "$(IMAGE):latest"
	docker push "$(IMAGE):latest"

.PHONY: run
run:
	poetry run python3 -m application_landscape_explorer
