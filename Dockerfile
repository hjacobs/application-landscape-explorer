FROM python:3.8-slim

WORKDIR /

RUN pip3 install poetry

COPY poetry.lock /
COPY pyproject.toml /

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-dev --no-ansi

FROM python:3.8-slim

WORKDIR /

# copy pre-built packages to this image
COPY --from=0 /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

# now copy the actual code we will execute (poetry install above was just for dependencies)
COPY application_landscape_explorer /application_landscape_explorer

ARG VERSION=dev

# replace build version in package and
# add build version to static asset links to break browser cache
# see also "version" in Makefile
RUN sed -i "s/^__version__ = .*/__version__ = \"${VERSION}\"/" /application_landscape_explorer/__init__.py && \
    sed -i "s/v=[0-9A-Za-z._-]*/v=${VERSION}/g" /application_landscape_explorer/templates/index.html

ENTRYPOINT ["/usr/local/bin/python", "-m", "application_landscape_explorer"]
