# Application Landscape Explorer

Explore your landscape of applications/services with this interactive web UI built with [d3.js](https://d3js.org/).
Application/service and dependency information must be provided as input data in TSV format.

![screenshot 2 (many services)](screenshot2.png)
![screenshot 3 (partitioning)](screenshot3.png)

Current features:

* search by application ID, team ID, or arbitrary tags
* hide nodes "far away" from the selection (press "1" to only show applications directly connected to the selection)
* keyboard shortcuts for pan/zoom (arrow keys, +/-, "C" to center on selection)
* color applications by owning team

Applications can represent any software component, not only microservices, e.g. background jobs, web frontends, or even an external SaaS provider.
Every application is identified by a unique ID. The ID can be any string, but should not contain whitespace or "special" characters (otherwise searching for the ID will be difficult for humans).
Each application is owned by one team. Teams are identified by string ID (again without whitespace).

## Usage

### Run with Docker

Run the web app with example data on port 8080:

```
docker run -it -p 8080:8080 hjacobs/application-landscape-explorer:latest
```

Open localhost:8080 in your browser and Use the "Help" button in the top right corner to learn about keyboard shortcuts.

###  Run from source

The backend requires Python 3.7+ and [Poetry](https://python-poetry.org/).

This will run the app with example data:
```
make install
make run
```

## Data

Data for applications and their dependencies can be provided by plain TSV files.
Example with dependencies between applications 'myapp', 'foo', and 'bar':

```
echo -e "myapp\tfoo" > mydependencies.tsv
echo -e "foo\tbar" >> mydependencies.tsv
echo -e "myapp\tbar" >> mydependencies.tsv
poetry run python3 -m application_landscape_explorer --dependencies-file mydependencies.tsv
```

The `--dependencies-file` argument takes a path to a TSV file with at least two columns, named `source` and `target`:

* `source`: source application ID ("caller", "client", "consumer")
* `target`: target application ID ("callee", "server", "provider")
* `type`: optional type of the dependency, e.g. "http" or "event"
* `tags`: comma-separated list of arbitrary `key:value` labels

The optional `--applications-file` argument expects a path to a TSV file with these columns (all of them except `id` are optional):

* `id`: unique application ID
* `name`: human-readable name of the application
* `team`: ID of the owning team
* `tier`: application's criticality
* `tags`: comma-separated list of arbitrary `key:value` labels

You can transform existing JSON files to TSV with `jq`, e.g. to take an JSON array with application objects:

```
echo -e "id\tname\tteam" > applications.tsv
cat applications.json | jq '.[]|[.id,.name,.team_id]|@tsv' -r >> applications.tsv
```

## URL Parameters

The frontend code reads a number of fragment parameters. Some of them enable hidden features which are not exposed in the UI.
Parameters are passed in the URL as `key=value` after the hash (`#`) symbol and separated by semicolon (`;`).

Available URL parameters:

* `q`: the search query (application ID, team ID, tag)
* `nodecolor`: how to color nodes, can be `team` (default) or `tag:<tag-prefix>` (e.g. `nodecolor=tag:dc` to color data centers differently)
* `partitionx`: comma-separated list of tags to partition the nodes on the horizontal axis (e.g. `partitionx=dc:dc1,dc:dc2` will move nodes of DC1 to the left and nodes of DC2 to the right)
* `partitiony`: comma-separated list of tags to partition the nodes on the vertical axis
